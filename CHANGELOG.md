# Changelog

## v0.3.0

Published 2018-08-16

- Build two versions, ES module and CommonJS.
    Bundling with Rollup and using CommonJS produced ugly code. Exporting in ES
    format allows Rollup to import as ES6 module and it looks better.

## v0.2.0

Published 2018-08-15.

- Add function `buildDom()`: build a DOM element starting from a recipe
    contained in a JavaScript object.

## v0.1.1

Published 2018-08-15.

- follow airbnb style guide

## v0.1

Published 2018-08-03.

First build, for now containing only:

- `getClog()`: customize `console.log()` with a custom prefix and allows to 
    disable console output on initialization.
