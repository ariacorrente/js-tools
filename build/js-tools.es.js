/**
 * @module js-tools
 * @description Small collection of various JavaScript tools.
 * @license GPL-3.0-or-later
 */

/**
 * Return a customized console.log function.
 *
 * The returned function configuration is determined on initialization. Options
 * available:
 *
 * - prepend a string to all messages;
 * - enable/disable the output
 *
 * @param {string} channelName - String to be prepended to all output messages.
 * @param {bool} debugEnabled - If `true` the console output is enabled.
 * @return {function} Customized console.log function.
 *
 * @example <caption>Print something</caption>
 * const clog = getClog('myApp', true);
 * clog('Starting my app');
 * // Will output 'myApp| Starting my app' to console.
 *
 * @example <caption>Disable output for the published version</caption>
 * const clog = getClog('myApp');
 * clog('Starting my app');
 * // Will produce no output to console.
 */
function getClog(channelName, debugEnabled = false) {
  let clog;

  if (debugEnabled) {
    clog = function customConsoleLog(...args) {
      let prefix;
      if (channelName !== undefined) {
        prefix = `${channelName}|`;
      } else {
        prefix = 'clog|';
      }
      console.log(prefix, ...args);
    };
  } else {
    clog = function nop() {};
  }
  return clog;
}

/**
 * Build a DOM element following the recipe contained in a JS object.
 *
 * The object properties can be:
 *
 * - `tag`: a string with the tag of the element
 *
 * - `eventSomething`: a function to be called when the event 'something' is
 *  triggered.
 *
 * - `children`: an object or an array of objects with the same structure of
 *  this one. The elements will be added as children of this object.
 *
 * - `text`: a string of text to be added as a child element of type TextNode.
 *  This is a shortcut to avoid to have to use "children" to create a simple
 *  TextNode.
 *
 * - Any HTML attribute: a string of the name of the attribute to be added to
 *   the element. For example 'class', 'id', 'href' etc...
 *
 * @param {object} data - Object containing the recipe for the DOM element to
 *  build.
 * @return {Element} The DOM element built.
 * @throws `Error` on empty or invalid object structure.
 *
 * @example <caption>H1 title</caption>
 * // To get <h1>First level caption</h1>
 * {
 *   'tag': 'h1',
 *   'text': 'First level caption'
 * }
 *
 * @example <caption> A DIV with class</caption>
 * // To get <div class="myClass"></div>
 * {
 *   'tag': 'div',
 *   'class': 'myClass'
 * }
 *
 * @example <caption>A minimal article</caption>
 * // To get <article><h1>Title</h1><p>Body</p></article>
 * {
 *   'tag': 'article',
 *   'children':
 *   [
 *    {
 *      'tag': 'h1'
 *    },
 *    {
 *      'tag': 'p',
 *      'text': 'Body'
 *    }
 *   ]
 * }
 *
 * @example <caption>A clickable button</caption>
 * // to get an interactive <button>Click me</button>
 * {
 *   'tag': 'button',
 *   'text': 'Click me',
 *   'eventClick': (event) => {
       console.log(event.target.innerHTML);
 *   }
 * }
 */
function buildDom(data) {
  let el;
  let children;

  // Check the root element
  if (!data) {
    // Empty data?
    throw (new Error('Empty data'));
  } else if (data.tag) {
    // Tag found, this is a typical HTML tag
    el = document.createElement(data.tag);
  } else if (data.text) {
    // No tag but a "text" means this is a text-node
    el = document.createTextNode(data.text);
    // No children for text nodes, return now
    return el;
  } else {
    // No tag or a text-field, invalid object
    throw (new Error('Data missing tag or text'));
  }

  // Iterate all the object keys for attributes and children
  const names = Object.keys(data);
  for (let i = 0; i < names.length; i += 1) {
    const name = names[i];
    if (name === 'tag') ; else if (name === 'text') {
      // Shortcut for textNodes instead of using a child they are identified
      // by the "text" object name
      const textNode = document.createTextNode(data.text);
      el.appendChild(textNode);
    } else if (name.startsWith('event')) {
      const eventName = name.substring(5).toLowerCase();
      el.addEventListener(eventName, data[name]);
    } else if (name === 'children') {
      ({ children } = data);
      if (Array.isArray(children)) {
        // If multiple children then it is an array and i must iterate
        children.forEach((item) => {
          // Build recursively all children
          if (item) {
            el.appendChild(buildDom(item));
          }
        });
      } else if (children) {
        // Only one child, call recursively
        el.appendChild(buildDom(children));
      }
    } else {
      // Add all other attributes
      el.setAttribute(name, data[name]);
    }
  }
  return el;
}

export { getClog, buildDom };
