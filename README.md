<a name="module_js-tools"></a>

## js-tools
Small collection of various JavaScript tools.

**License**: GPL-3.0-or-later  

* [js-tools](#module_js-tools)
    * [~getClog(channelName, debugEnabled)](#module_js-tools..getClog) ⇒ <code>function</code>
    * [~buildDom(data)](#module_js-tools..buildDom) ⇒ <code>Element</code>

<a name="module_js-tools..getClog"></a>

### js-tools~getClog(channelName, debugEnabled) ⇒ <code>function</code>
Return a customized console.log function.

The returned function configuration is determined on initialization. Options
available:

- prepend a string to all messages;
- enable/disable the output

**Kind**: inner method of [<code>js-tools</code>](#module_js-tools)  
**Returns**: <code>function</code> - Customized console.log function.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| channelName | <code>string</code> |  | String to be prepended to all output messages. |
| debugEnabled | <code>bool</code> | <code>false</code> | If `true` the console output is enabled. |

**Example** *(Print something)*  
```js
const clog = getClog('myApp', true);
clog('Starting my app');
// Will output 'myApp| Starting my app' to console.
```
**Example** *(Disable output for the published version)*  
```js
const clog = getClog('myApp');
clog('Starting my app');
// Will produce no output to console.
```
<a name="module_js-tools..buildDom"></a>

### js-tools~buildDom(data) ⇒ <code>Element</code>
Build a DOM element following the recipe contained in a JS object.

The object properties can be:

- `tag`: a string with the tag of the element

- `eventSomething`: a function to be called when the event 'something' is
 triggered.

- `children`: an object or an array of objects with the same structure of
 this one. The elements will be added as children of this object.

- `text`: a string of text to be added as a child element of type TextNode.
 This is a shortcut to avoid to have to use "children" to create a simple
 TextNode.

- Any HTML attribute: a string of the name of the attribute to be added to
  the element. For example 'class', 'id', 'href' etc...

**Kind**: inner method of [<code>js-tools</code>](#module_js-tools)  
**Returns**: <code>Element</code> - The DOM element built.  
**Throws**:

- `Error` on empty or invalid object structure.


| Param | Type | Description |
| --- | --- | --- |
| data | <code>object</code> | Object containing the recipe for the DOM element to  build. |

**Example** *(H1 title)*  
```js
// To get <h1>First level caption</h1>
{
  'tag': 'h1',
  'text': 'First level caption'
}
```
**Example** *( A DIV with class)*  
```js
// To get <div class="myClass"></div>
{
  'tag': 'div',
  'class': 'myClass'
}
```
**Example** *(A minimal article)*  
```js
// To get <article><h1>Title</h1><p>Body</p></article>
{
  'tag': 'article',
  'children':
  [
   {
     'tag': 'h1'
   },
   {
     'tag': 'p',
     'text': 'Body'
   }
  ]
}
```
**Example** *(A clickable button)*  
```js
// To get an interactive <button>Click me</button>
{
  'tag': 'button',
  'text': 'Click me',
  'eventClick': (event) => {
    console.log(event.target.innerHTML);
  }
}
```
