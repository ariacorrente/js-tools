const expect = require('unexpected');
const jsTools = require('../build/js-tools.cjs.js');

const { getClog } = jsTools;

/*
* Overwrite the function console.log() to redirect output to a local variable
* string to allow the inspection of the output of clog();
*/
const consoleLog = console.log;
let consoleOutput = '';
function overwriteConsole() {
  console.log = function consoleLogWrapper(...args) {
    consoleOutput = '';
    for (let i = 0; i < args.length; i += 1) {
      consoleOutput += args[i];
    }
    // consoleLog.apply(undefined, arguments);
  };
}
/*
 * Must restore the original console because it is used (by mocha?) to output
 * the test results. If not restored the consoleOutput variable will be
 * filled with the test results and the tests will fail.
 */
function restoreConsole() {
  console.log = consoleLog;
}

describe('Test js-tools', () => {
  describe('Testing test framework', () => {
    it('1 should be 1', () => {
      expect(1, 'to be', 1);
    });
  });

  describe('getClog()', () => {
    it('should be a function', () => {
      expect(getClog, 'to be a', 'function');
    });
    it('should not print', () => {
      const clog = getClog('moduleName');
      overwriteConsole();
      clog('ciao');
      restoreConsole();
      expect(consoleOutput, 'to be', '');
    });
    it('should not print', () => {
      const clog = getClog('asd');
      overwriteConsole();
      clog('ciao');
      restoreConsole();
      expect(consoleOutput, 'to be', '');
    });
    it('should use default prefix', () => {
      const clog = getClog(undefined, true);
      overwriteConsole();
      clog('ciao');
      restoreConsole();
      expect(consoleOutput, 'to be', 'clog|ciao');
    });
    it('should print', () => {
      const clog = getClog('moduleName', true);
      overwriteConsole();
      clog('ciao');
      restoreConsole();
      expect(consoleOutput, 'to be', 'moduleName|ciao');
    });
  });
});
