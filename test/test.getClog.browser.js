import { getClog } from '../build/js-tools.es';

/*
 * Overwrite the function console.log() to redirect output to a local variable
 * string to allow the inspection of the output of clog();
 */
const consoleLog = window.console.log;
let consoleOutput = '';
window.console.log = function consoleLogWrapper(...args) {
  consoleOutput = '';
  for (let i = 0; i < args.length; i += 1) {
    consoleOutput += args[i];
    consoleLog(args);
  }
};

describe('getClog()', () => {
  it('should be a function', () => {
    expect(getClog, 'to be a', 'function');
  });
  it('should not print', () => {
    const clog = getClog('moduleName');
    clog('ciao');
    expect(consoleOutput, 'to be', '');
  });
  it('should not print', () => {
    const clog = getClog('asd');
    clog('ciao');
    expect(consoleOutput, 'to be', '');
  });
  it('should use default prefix', () => {
    const clog = getClog(undefined, true);
    clog('ciao');
    expect(consoleOutput, 'to be', 'clog|ciao');
  });
  it('should print', () => {
    const clog = getClog('moduleName', true);
    clog('ciao');
    expect(consoleOutput, 'to be', 'moduleName|ciao');
  });
  it('should print only prefix', () => {
    const clog = getClog('moduleName', true);
    clog('');
    expect(consoleOutput, 'to be', 'moduleName|');
  });
});
