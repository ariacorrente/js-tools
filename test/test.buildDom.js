import { buildDom } from '../src/js-tools';

const dataH1 = {
  tag: 'h1',
};

describe('buildDom()', () => {
  it('should be a function', () => {
    expect(buildDom, 'to be a', 'function');
  });

  it('should throw on empty object', () => {
    expect(() => { buildDom({}); }, 'to throw');
    expect(() => { buildDom(); }, 'to throw');
    expect(() => { buildDom(null); }, 'to throw');
    expect(() => { buildDom(undefined); }, 'to throw');
  });

  it('should be an <H1>', () => {
    expect(buildDom(dataH1).tagName, 'to be', 'H1');
  });

  it('should handle on an undefined children', () => {
    dataH1.children = undefined;
    expect(() => { buildDom(dataH1); }, 'not to throw');
  });

  it('should handle an [] children', () => {
    dataH1.children = [];
    expect(() => { buildDom(dataH1); }, 'not to throw');
  });

  it('should throw on an {} children', () => {
    dataH1.children = {};
    expect(() => { buildDom(dataH1); }, 'to throw');
  });

  it('should throw on an [{}] children', () => {
    dataH1.children = [{}];
    expect(() => { buildDom(dataH1); }, 'to throw');
  });

  it('should return a text node', () => {
    const data = {
      text: 'Some test',
    };
    expect(buildDom(data), 'to be a', Text);
  });

  it('should return a <div>', () => {
    const data = {
      tag: 'div',
    };
    expect(buildDom(data), 'to be a', HTMLDivElement);
  });

  it("should return an element with class 'class-name'", () => {
    const data = {
      tag: 'div',
      class: 'class-name',
    };
    expect(buildDom(data).className, 'to be', 'class-name');
  });

  it('should return a <p> with the correct text', () => {
    const data = {
      tag: 'p',
      text: 'Text of paragraph',
    };

    const node = buildDom(data);
    expect(node, 'to be a', HTMLParagraphElement);
    expect(node.textContent, 'to be', 'Text of paragraph');
  });

  it('should call the bound event handler', () => {
    const event = new Event('build');
    const eventHandler = (e) => {
      e.cb();
    };
    const data = {
      tag: 'button',
      eventBuild: eventHandler,
    };
    const node = buildDom(data);
    const functionWrapper = (cb) => {
      event.cb = cb;
      node.dispatchEvent(event);
    };

    expect(functionWrapper, 'to call the callback');
  });

  it('should return a <p> nested in a <div', () => {
    const data = {
      tag: 'div',
      children: {
        tag: 'p',
      },
    };
    const node = buildDom(data);

    expect(node, 'to be a', HTMLDivElement);
    expect(node.childElementCount, 'to be', 1);
    expect(node.firstChild, 'to be a', HTMLParagraphElement);
  });

  it('should return a <ul> with 10 children <li>', () => {
    const data = {
      tag: 'ul',
      children: [],
    };

    for (let i = 0; i < 10; i += 1) {
      data.children[i] = {
        tag: 'li',
        text: `item number${i}`,
      };
    }
    const node = buildDom(data);
    expect(node, 'to be a', HTMLUListElement);
    expect(node.childElementCount, 'to be', 10);
    for (let i = 0; i < node.childElementCount; i += 1) {
      expect(node.children[i], 'to be a', HTMLLIElement);
    }
  });

  it('should return 20 nested <span>', () => {
    const data = {};

    let currentLevel = {};
    const count = 3;
    currentLevel = data;

    for (let i = 0; i < count; i += 1) {
      currentLevel.tag = 'span';
      currentLevel.text = `level: ${i}`;
      currentLevel.children = [];
      if (i < count - 1) {
        currentLevel.children[0] = {};
        // Destructuring assignment for currentLevel.children[0]
        [currentLevel] = currentLevel.children;
      }
    }
    const node = buildDom(data);
    expect(node, 'to be a', HTMLSpanElement);
    expect(node.childElementCount, 'to be', 1);
    let children = node;
    for (let i = 0; i < count; i += 1) {
      for (let j = 0; j < children.childNodes.length; j += 1) {
        if (children.childNodes[j].tagName === 'SPAN') {
          expect(children, 'to be a', HTMLSpanElement);
          if (i < count - 1) {
            expect(children.childElementCount, 'to be', 1);
          } else {
            expect(children.childElementCount, 'to be', 0);
          }
          children = children.childNodes[j];
        }
      }
    }
  });

  it('should return a typical article', () => {
    const path = 'https://some-domain.com/some-image.jpg';
    const className = 'wide';
    const paragraph = 'A small paragraph.';

    const data = {
      tag: 'article',
      children: [
        {
          tag: 'h1',
          text: 'Article title',
        },
        {
          tag: 'img',
          class: className,
          href: path,
          alt: 'A description',
        },
        {
          tag: 'p',
          text: paragraph,
        },
        {
          tag: 'p',
          text: "A veeeeeeeeeeeeeeeeery veeeeeeeeeeeeeery loooooong text. But I meean veeeeeeeeeeeery looooooooooooong, i'm nooooooooooot joooooking.",
        },
      ],
    };

    const node = buildDom(data);
    expect(node.tagName, 'to be', 'ARTICLE');
    expect(node.childElementCount, 'to be', 4);
    expect(node.childNodes[0].tagName, 'to be', 'H1');
    expect(node.childNodes[1].tagName, 'to be', 'IMG');
    expect(node.childNodes[2].tagName, 'to be', 'P');
    expect(node.querySelector(`.${className}`).getAttribute('href'), 'to be', path);
    expect(node.childNodes[3].tagName, 'to be', 'P');
    expect(node.childNodes[2].textContent, 'to be', paragraph);
  });
});
