ROLLUP = node_modules/.bin/rollup
MOCHA = node_modules/.bin/mocha
ESLINT = node_modules/.bin/eslint

.PHONY: test eslint eslint-fix

build: build/js-tools.cjs.js \
	build/js-tools.es.js

# CommonJS version, for nodejs
build/js-tools.cjs.js: src/js-tools.js
	 $(ROLLUP) $< --file $@ --format cjs

# ES Module version
build/js-tools.es.js: src/js-tools.js
	 $(ROLLUP) $< --file $@ --format esm

# The readme is built from docs in comments using jsdoc
doc: README.md

clean:
	rm -rf ./build
	rm -rf ./build-test

#
# CONFIGURE DEVELOPMENT ENVIRONMENT
#
init-dev-env: .git/hooks/pre-commit

# Link to git pre-commit hook and make executable
.git/hooks/pre-commit: script/pre-commit.sh
	ln -s `pwd`/$? $@
	chmod +x $@

# Build Documentation
README.md: src/js-tools.js
	mkdir -p build-doc
	node_modules/.bin/jsdoc2md $< > $@

#
# ESLINT
#
eslint:
	$(ESLINT) src/ test/

eslint-fix:
	$(ESLINT) --fix src/ test/

#
# TESTS
#
test: build-test
	xdg-open build-test/test.html
	$(MOCHA) test/test.getClog.nodejs.js

build-test: build \
	build-test/test.html \
	build-test/test.getClog.browser.js \
	build-test/test.buildDom.js

build-test/test.html: test/test.html
	mkdir -p build-test
	cp test/test.html build-test/

build-test/test.getClog.browser.js: test/test.getClog.browser.js

build-test/test.buildDom.js: test/test.buildDom.js

#
# IMPLICIT RULES
#
# Build tests with rollup
build-test/%.js: test/%.js
	$(ROLLUP) $< --file $@ --format iife
